---
layout: markdown_page
title: "Marketing Programs Process Details"
---

# Purpose
The purpose of this page is to capture the detail-by-detail processes executed by the Marketing Programs team. This includes integrated campaigns, nurture programs, gated content, event tracking, landing page creation, email sends, and more. For an overview of the team and priorities, see the [Marketing Programs Management handbook page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/). If you have any questions please post in the [#marketing_programs Slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

## 📌 How to gate a piece of content

#### 1️⃣  Start with the Landing Page on about.gitlab
1. Navigate to the [/resources/ repo](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/resources)
1. Click the `+` dropdown and select `New Directory`
1. Add the directory name using the following syntax: `[type]-short-name`
   * *Name is very important as this is the url for the landing page!*
   * Keep it short, and use hyphens between words. (i.e. `ebook-ciso-secure-software`)
1. Add commit message to name your Merge request using syntax `Add [content type] landing page - [name of content]` (i.e. `Add ebook landing page - CISO Secure Software`)
1. Create a name for the target branch - NEVER leave it as the master (i.e. `jax-ebook-ciso`)
1. On the next screen (New Merge Request), add `WIP: ` to the beginning of the title and add a quick description (`Add LP for [content name]` will suffice)
1. Assign to Jackie Gragnola and scroll down, check the box for “Delete source branch when merge request is accepted”
1. Click `Submit Merge Request`
1. You’ve now created the merge request, but you need to add the file (code) for the landing page itself within the directory
1. Click on the link to the right of “Request to Merge” and you will enter the branch you’ve created with your MR
1. Click the `Source` folder, then `Resources` and click into the new directory as you named it
1. Click the `+` dropdown and select `New File`
1. Where it says “File Name” at the top, type in `index.html.haml`
1. Copy the following code: (see below)

```
---
layout: default
title: `add title here, don’t use colons`
suppress_header: true
extra_css:
  - styles-2018.css
destination_url: "`add the pathfactory url when available&lb_email="
form_id: "1002"
form_type: "resources"
cta_title: "Download the `type: eBook, etc`"
cta_date: 
cta_subtitle: 
link_text: "Click here to download the `type: eBook, etc`."
success_message: "You will also receive a copy of the `type: eBook, etc`sent to your inbox shortly."
---

.wrapper
  .page-illustrated-header-container
    = partial "includes/icons/gitlab-icon-pattern-header.svg"
    .container
      .header-container-content
        %h1.page-headline `add headline from copy doc`
        %h2.page-headline `add subhead from copy doc`

  .content-container
    .wrapper.container{ role: "main" }
      .row
        .col-xs-12.col-md-6.col-md-offset-1
          .content-section
            %p `Add P1 from copy doc`
            
            %p `Add P2 from copy doc`

            %h3 What you’ll learn in this `type: eBook, etc.`:
            %ul
              %li `add bullet 1`
              %li `add bullet 2`
              %li `add bullet 3`

            %p `add closing paragraph`

        .col-md-4.col-md-offset-1
          = partial "includes/form-to-resource", locals: { destination_url: current_page.data.destination_url, form_id: current_page.data.form_id, form_type: current_page.data.form_type, cta_title: current_page.data.cta_title, cta_date: current_page.data.cta_date, cta_subtitle: current_page.data.cta_subtitle, link_text: current_page.data.link_text, success_message: current_page.data.success_message }
```

1. Back in your MR, paste, the code where the file begins with 1.
1. Update the variables in the codeIn all the places in `snippet code`
  * *Be sure to remove the back-tick code symbols* so that the copy `does not turn red on the landing page`. This is just to be used as a guideline while editing the landing page.
1. If you don’t yet have a pathfactory link, leave the copy in that place. When you have  the Pathfactory link in a later step, you’ll edit the MR with the new link before pushing live (see later step)
1. Important! If there is a colon `:` in your title, replace with a dash `-` for the page title, as it will break the page.
1. In the Commit Message below your code, add a note that you’re adding the code for the landing page
1. Click `Commit Changes`
1. Now, while you wait for the pipeline to approve, move onto the next step in Marketo to facilitate the flows and tracking of the program.

#### 2️⃣  Create Marketo Program
1. Clone the [gated content template](https://app-ab13.marketo.com/#PG2524A1) in Marketo
1. Choose clone to `A campaign folder`
1. Use naming syntax `[yyyy]_[type]_NameOfAsset` (i.e. 2019_eBook_CISOSecureSoftware) - keep this short
1. Add to folder: `4 - Content Marketing`
1. Leave description blank and select “create”
1. When the new program loads, copy the url of the Marketo program and add to the description of the gating issue under `DRIs and Links`

#### 3️⃣  Create Salesforce Program
1. In the program summary, under “Settings” next to `Salesforce Campaign Sync` click on “not set”
1. In the popup that appear, click the down arrow and select `Create New`
1. Leave the name as it pre-fills so that it will exactly match the program name in Marketo
1. In the Description, write `Epic: ` and link to the content Epic url
1. Click save, and navigate to the [campaigns view](https://gitlab.com/groups/gitlab-com/marketing/-/epics/400) in Salesforce
1. Choose the “Gated Content” campaign view and make sure it is in order by Created Date (first column) from latest to earliest creation. Your campaign should appear at the top.
1. Click into your campaign and change the owner to your name
1. Change the `Type Details` field to `Gated` and save
1. Copy the url of the Salesforce campaign and add to the description of the gating issue under `DRIs and Links`

#### 4️⃣  Update the Marketo Tokens
1. Navigate to the `My Tokens` section of the Marketo program, and make the following edits:
  * Content Download URL: add the PathFactory link after the asset is loaded into Pathfactory and added to a track. This is provided by the Pathfactory DRI.
  * Content Epic URL: the url to the overall epic of the content
  * Content Title: the content title as it appears on the asset - not necessarily what is displayed as the landing page header on the copy doc (i.e. `10 Steps Every CISO Should Take to Secure Next-Gen Software`)
  * Content Type: select the appropriate content type (i.e. leave just `eBook`)
  * UTM: leave the code that is originally there, and at the end of the code, add the name of the Marketo program, removing any underscores or special characters (i.e. `2019eBookCISOSecureSoftware`)
  * Copy and paste the 3 bullets from the content copy document into the 3 tokens aligned to the bullets

#### 5️⃣  Preview the emails
1. Preview the confirmation email to make sure that the tokens appear correctly.
1. If you haven’t uploaded to Pathfactory and received a final link, your button will be broken. When you add that to the Marketo tokens, the button will work properly.

#### 6️⃣  Edit, Review, and Activate Marketo Smart Campaign
1. In the `01 Downloaded Content` smart campaign, under `Smart List`add the url created in step 1 to the `/resources/` link in the referrer constraint (i.e. add `ebook-ciso-secure-software` after`/resources/` so that it is `/resources/ebook-ciso-secure-software`
  * In the `Flow` section, review the steps. It should include:
  * Send Email: Confirmation email from your program (this automatically pulls in)
  * Remove from flow with “choice” that if email address contains `@gitlab.com` it should not continue the steps
  * Change program status in your program to `Gated Content  > Downloaded`
  * Interesting moment using tokens with “Web” type, and description: `Downloaded the {{my.content type}}: “{{my.Content Title}}`
  * Send alert to `None`and {{my.mpm owner email address}} (reps receive MQL alerts, so this only triggers to the MPM and can be filtered out of your email if desired).
  * Change data value if `Acquisition Program` is empty to now be the current program
  * Change data value if `Person Source` is empty to be “Gated Content - {{my.content type}}”
  * Change data value if `Initial Source` is empty to be “Gated Content - {{my.content type}}”
  * Change data value if `Person Status` is empty to be “Inquiry”
1. In the Schedule section, check that it’s set for each person to run through the flow every 7 days and click `Activate`

#### 7️⃣  Test Your Page in the Review App
1. Navigate back to your MR in GitLab
1. When your pipeline has complete, click “View App” and add the URL you chose for your page (i.e. add `/resources/ebook-ciso-secure-software`to the end of the view app url
1. Fill out the form using your test lead (using email syntax [your email]+[name]@gitlab.com - i.e. `jgragnola+george@gitlab`)
1. Check in the Marketo program that it triggered the autoresponder, addition to the program, and alert.
1. In your inbox, check the confirmation copy and urls.
1. If this fires properly, return to the MR and assign to Jackie with a comment to ping her and let her know it’s ready to merge.
1. Check off all completed actions on the issue.
1. When the MR merges into the master, it will appear at the URL you determined.
1. When it has appeared, test it once more with a different test lead email address, and confirm that everything fires as desired.

#### 🏁  Notify the Team
1. Comment in the issue with the final URL and a message that the page is now live and working as intended.
1. Close out the issue.

## 📌  How to set up a Webcast (technical, in partnership with Product Marketing)
1. TBD

## 📌  How to setup of an Event (technical, in partnership with Field Marketing)
1. TBD1